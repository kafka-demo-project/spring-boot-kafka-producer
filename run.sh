docker rm -f app-producer
docker build --no-cache -t app-producer .
docker run --name app-producer --network=host -d -p 8080:8081 app-producer
docker logs -f app-producer