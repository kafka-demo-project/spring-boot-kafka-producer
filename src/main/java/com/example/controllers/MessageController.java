package com.example.controllers;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Message;
import com.example.model.MessageResponse;
import com.example.service.KafkaSender;

@CrossOrigin
@RestController
public class MessageController {
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired
	private KafkaSender kafkaSender;

	@PostMapping("/messager")
	public MessageResponse receiveMsg(@RequestBody Message msg) {

		LocalDateTime receiveTime = LocalDateTime.now();
		msg.setReceiveTime(receiveTime.toString());

		// publish msg
		kafkaSender.send(msg);
		// response
		return new MessageResponse("OK", receiveTime.toString());
	}

	@GetMapping("/")
	public String home() {
		logger.info("GET /home");
		return "HELLO KAFKA";
	}

}
