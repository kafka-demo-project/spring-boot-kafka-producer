package com.example.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import com.example.model.Message;

@Service
public class KafkaSender {

	private static final Logger log = LoggerFactory.getLogger(KafkaSender.class);

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@Value("${sender.topic-name}")
	private String topicName;

	public void send(Message message) {
		Map<String, Object> headers = new HashMap<>();
		headers.put(KafkaHeaders.TOPIC, topicName);
		kafkaTemplate.send(new GenericMessage<Message>(message, headers));
		// use the below to send String values through kafka
//		kafkaTemplate.send(topicName, "example string value");
//		log.info("Data - " + message.toString() + " sent to Kafka Topic - " + topicName);
	}
}
