package com.example.model;

import lombok.Data;

@Data
public class MessageResponse {
	private String code;
	private String receiveTime;

	public MessageResponse(String code, String receiveTime) {
		super();
		this.code = code;
		this.receiveTime = receiveTime;
	}

	public MessageResponse() {
		super();
	}

}
